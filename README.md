# Deploying Spark on Kubernetes

First, we should start our K8s cluster. In our case we used Minikube.

It is recommended to configure Minikube to use 8 GB of memory and 4 CPU cores when running Spark jobs on Kubernetes.

```minikube start --memory 8192 --cpus 4```

Next, let's build a custom Docker image for Spark 3.0.0, designed for Spark Standalone mode.

```docker build -f docker/Dockerfile -t spark-hadoop:3.0.0 ./docker```

To verify that the image was built successfully

```docker image ls spark-hadoop```

Now, we create the Spark master **Deployment** and start the Spark master **Services**

```kubectl create -f ./kubernetes/spark-master-deployment.yaml```

```kubectl create -f ./kubernetes/spark-master-service.yaml```

Next, we create the Spark worker **Deployment**

```kubectl create -f ./kubernetes/spark-worker-deployment.yaml```

Finally, to verify that the Spark master and workers were configured correctly

```kubectl get deployments```

```kubectl get pods```

# Access Spark UI and Spark Shell

To access the Spark Web UI, we run the command

```minikube service spark-master```

Now, to run Spark Shell within our Spark master pod.

```kubectl exec <MASTER-POD-NAME> -it -- pyspark --conf spark.driver.bindAddress=<MASTER-IP> --conf spark.driver.host=<MASTER-IP>```

- Spark Master pod name is found in ```kubectl get pods```

- Spark Master IP is found in ```kubectl get pods -o wide``` under the column **IP**

# Test

Run the following code after the PySpark prompt appears.

```
words = 'the quick brown fox jumps over the lazy dog the quick brown fox jumps over the lazy dog'
seq = words.split()
data = sc.parallelize(seq)
counts = data.map(lambda word: (word, 1)).reduceByKey(lambda a, b: a + b).collect()
dict(counts)
sc.stop()
```

If everything is working properly, you should see this result.

```{'brown': 2, 'lazy': 2, 'over': 2, 'fox': 2, 'dog': 2, 'quick': 2, 'the': 4, 'jumps': 2}```